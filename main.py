import argparse
import os

import cv2
import numpy as np

from usm.color.rgb2yuv import rgb2yuv444p
from usm.color.yuv2rgb import yuv444p2rgb
from polyblur.polyblur import PolyBlur
from usm.filters.guided_filter import guided_filter
from usm.usm import USM

def main(args):

    os.makedirs(args.out_dir, exist_ok=True)
    files = os.listdir(args.root_dir)
    for file in files:
        print(file)

        if not file.endswith('.jpg'):
            continue

        name = file.split('.')[0]
        path = os.path.join(args.root_dir, file)
        img = cv2.imread(path)

        # pre-filtering: RGB -> YUV -> Y (H, W)
        y, u, v = rgb2yuv444p(img[:, :, 2], img[:, :, 1], img[:, :, 0])
        y_filter = 255 * np.clip(guided_filter(y / 255.0, y / 255.0, 3, 0.0001), -1.0, 1.0)
        residual = y - y_filter

        # PolyBlur: Y (H, W) -> Y (H, W, 1) -> Y (H, W)
        y_deblur, psf = PolyBlur.deblur(y_filter[:, :, np.newaxis] / 255.0, iters=1, alpha=16, b=1, halo_remove=False)
        y_deblur = np.uint8(255 * np.clip(y_deblur[:, :, 0], 0, 1))

        # USM: Y (H, W) -> Y (H, W)
        y_sharp = USM.sharpen(y_deblur, strength=1.0, cut_off=0.00075, halo_pos=0.5, halo_neg=0.5, filtering=True)
        print(y_sharp.shape, y_sharp.min(), y_sharp.max())
        
        #Add Back Residual
        y_sharp = y_sharp + 1.0 * residual

        # Y (H, W) -> YUV -> RGB -> BGR
        rgb = yuv444p2rgb(y_sharp, u, v)
        cv2.imwrite(f"{args.out_dir}/{name}_{args.suffix}.png", np.uint8(rgb[:, :, ::-1]))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="NightSight Sharpening entry point") 
    parser.add_argument("--root_dir", type=str, help="Path to folder with jpg or png images to be sharpened")   
    parser.add_argument("--out_dir", type=str, help="Path to folder to save output sharpened images")
    parser.add_argument("--suffix", type=str, help="String to add at the end of the output sharpened images", default="")
    args = parser.parse_args()

    main(args)