import numpy as np
import cv2


def box_filter(imSrc, r):
    # Box Filter   O(1) time box filtering using cumulative sum
    #
    # - Definition imDst(x, y)=sum(sum(imSrc(x-r:x+r,y-r:y+r)));
    # - Running time independent of r;
    # - Equivalent to the function: colfilt(imSrc, [2*r+1, 2*r+1], 'sliding', @sum);
    # - But much faster.

    return cv2.boxFilter(imSrc, -1, (r, r))
