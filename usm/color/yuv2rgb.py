import numpy as np
import cv2


def yuv420p2bgr_opencv(y, u, v):
    h, w = y.shape

    shape = (int(h * 1.5), w)
    yuv = np.zeros(shape, dtype=np.uint8)
    yuv[:h, :] = np.uint8(y)
    yuv[h:, 0::2] = np.uint8(u)
    yuv[h:, 1::2] = np.uint8(v)

    bgr = cv2.cvtColor(yuv, cv2.COLOR_YUV2BGR_NV21)
    return np.uint8(bgr)


def yuv420p2rgb(imgY, u, v):
    H, W = imgY.shape
    Cb = cv2.resize(np.uint8(u), (W, H), cv2.INTER_CUBIC) - 128.0
    Cr = cv2.resize(np.uint8(v), (W, H), cv2.INTER_CUBIC) - 128.0

    M = [[256.0, 0,     359.0],
         [256.0, -88.0, -183.0],
         [256.0, 454.0, 0.0]]

    RGB = np.zeros(shape=(H, W, 3), dtype=np.float64)
    RGB[:, :, 0] = M[0][0] * imgY + M[0][1] * Cb + M[0][2] * Cr
    RGB[:, :, 1] = M[1][0] * imgY + M[1][1] * Cb + M[1][2] * Cr
    RGB[:, :, 2] = M[2][0] * imgY + M[2][1] * Cb + M[2][2] * Cr

    return np.uint8(np.clip(RGB / 256, 0, 255))


def yuv444p2rgb(imgY, u, v):
    H, W = imgY.shape
    Cb = u - 128.0
    Cr = v - 128.0

    M = [[256.0, 0,     359.0],
         [256.0, -88.0, -183.0],
         [256.0, 454.0, 0.0]]

    RGB = np.zeros(shape=(H, W, 3), dtype=np.float64)
    RGB[:, :, 0] = M[0][0] * imgY + M[0][1] * Cb + M[0][2] * Cr
    RGB[:, :, 1] = M[1][0] * imgY + M[1][1] * Cb + M[1][2] * Cr
    RGB[:, :, 2] = M[2][0] * imgY + M[2][1] * Cb + M[2][2] * Cr

    return np.uint8(np.clip(RGB / 256, 0, 255))
