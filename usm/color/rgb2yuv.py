import numpy as np
import cv2


def rgb2yuv444p(r, g, b):
    mat = [[77.0, 150.0, 29.0],
           [-43.0, -85.0, 128.0],
           [128.0, -107.0, -21.0]]
    offset = [0, 128, 128]

    y = np.clip((r * mat[0][0] + g * mat[0][1] + b * mat[0][2]) / 256 + offset[0], 0, 255)
    u = np.clip((r * mat[1][0] + g * mat[1][1] + b * mat[1][2]) / 256 + offset[1], 0, 1023)
    v = np.clip((r * mat[2][0] + g * mat[2][1] + b * mat[2][2]) / 256 + offset[2], 0, 1023)
    return np.uint8(y), np.uint8(u), np.uint8(v)


def rgb2yuv420p(r, g, b):
    y, u, v = rgb2yuv444p(r, g, b)

    # yuv to yuv420p
    outcb = np.clip((u[0::2, 0::2] + u[0::2, 1::2] + u[1::2, 0::2] + u[1::2, 1::2]) / 4.0, 0, 255)
    outcr = np.clip((v[0::2, 0::2] + v[0::2, 1::2] + v[1::2, 0::2] + v[1::2, 1::2]) / 4.0, 0, 255)

    return np.uint8(y), np.uint8(outcb), np.uint8(outcr)