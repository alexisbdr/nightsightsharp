from usm.color.rgb2yuv import rgb2yuv444p
from usm.color.yuv2rgb import yuv444p2rgb
from usm.filters.guided_filter import guided_filter
import numpy as np
import cv2
import os


class USM:
    """ Un-sharp masking for image Y-channel sharpening """
    @staticmethod
    def sharpen(luma, strength=1.0, cut_off=0.0, halo_pos=0.8, halo_neg=1.0, filtering=False):
        """
        Y channel sharpening
        :param luma: np.array, image Y channel, np.uint8
        :param strength: float, sharpening strength, "0" -> no sharpening
        :param cut_off: float, suppress noise boost, "0" -> no noise suppression, "1" -> maximum noise suppression
        :param halo_pos: float, control positive gain, "0" -> no halo control, "1" -> max strength
        :param halo_neg: float, control negative gain, "0" -> no halo control, "1" -> max strength
        :param filtering: Bool, apply guided filtering on the detail layer to suppress unwanted noise boost
        """
        # compute gain map
        luma_base = cv2.GaussianBlur(luma, (7, 7), 3)
        luma_detail = 1.0 * luma - luma_base

        if filtering:
            luma_detail = 255 * np.clip(guided_filter(luma_detail / 255.0, luma_detail / 255.0, 3, 0.0001), -1.0, 1.0)

        luma_detail = 255 * USM.coring(luma_detail / 255, cut_off)
        luma_gain = strength * luma_detail

        census_mask = USM.census_transform(luma)
        luma_gain = census_mask * luma_gain

        # halo control
        luma_gain = USM.halo_control(luma, luma_gain, halo_pos, halo_neg)

        output = np.uint8(np.clip(luma + luma_gain, 0, 255))
        return output

    @staticmethod
    def edges(luma):
        """
        Compute edge map
        :param luma: np.array, image Y channel, np.uint8
        :return: np.array, np.float32 [0, 1]
        """
        # three direction gradient maps: horizontal, vertical, diagonal
        kernel = np.array([[1.0, 0, -1.0], [2.0, 0, -2.0], [1.0, 0, -1.0]], dtype=float) / 8.0
        gx = cv2.filter2D(1.0 * luma, -1, kernel)
        gxx = gx * gx
        kernel = np.array([[1.0, 2.0, 1.0], [0, 0, 0], [-1.0, -2.0, -1.0]], dtype=float) / 8.0
        gy = cv2.filter2D(1.0 * luma, -1, kernel)
        gyy = gy * gy
        gxy = gx * gy

        # edge map
        gxx = cv2.boxFilter(gxx, -1, (1, 1))
        gyy = cv2.boxFilter(gyy, -1, (1, 1))
        gxy = cv2.boxFilter(gxy, -1, (1, 1))
        edges = ((gxx + gyy) + np.sqrt((gxx - gyy) * (gxx - gyy) + 4 * gxy * gxy)) / 2

        # normalization
        output = np.tanh(edges / 255)

        return output

    @staticmethod
    def coring(detail, cut_off=0.0):
        """
        Remove noises with small gradient, values below the cut-off value will be set to "0"
        :param detail: np.array, np.float32 [0, 1]
        :param cut_off: float, suppress noise boost, "0" -> no noise suppression, "1" -> maximum noise suppression
        :return:
        """
        # make sure we do not cut off too many real edges
        cut_off = 0.3 * min(max(cut_off, 0.0), 1.0)
        c1, c2 = cut_off, 0.9 * cut_off + 0.1 * 1.0

        # values above c2 will remain the same
        output = detail

        # zero-out values below c1
        output[np.abs(detail) < c1] = 0

        # # re-map values between c1 and c2, linear mapping
        k = c2 / (c2 - c1)
        b = -k * c1

        mask = (detail >= c1) & (detail <= c2)
        output[mask] = k * detail[mask] + b

        mask = (detail >= -c2) & (detail <= -c1)
        output[mask] = -(k * np.abs(detail[mask]) + b)

        return output

    @staticmethod
    def census_transform(luma):
        """
        Compute mask for structure regions, used to supress noise boost in homogeneous regions
        :param luma: np.array, image Y channel, np.uint8
        :return:
        """
        threshold = 1
        masks = [np.uint8(np.abs(luma[1:-1, 1:-1] - luma[:-2, :-2]) > threshold),
                 np.uint8(np.abs(luma[1:-1, 1:-1] - luma[:-2, 1:-1]) > threshold),
                 np.uint8(np.abs(luma[1:-1, 1:-1] - luma[:-2, 2:]) > threshold),
                 np.uint8(np.abs(luma[1:-1, 1:-1] - luma[1:-1, :-2]) > threshold),
                 np.uint8(np.abs(luma[1:-1, 1:-1] - luma[1:-1, 2:]) > threshold),
                 np.uint8(np.abs(luma[1:-1, 1:-1] - luma[2:, :-2]) > threshold),
                 np.uint8(np.abs(luma[1:-1, 1:-1] - luma[2:, 1:-1]) > threshold),
                 np.uint8(np.abs(luma[1:-1, 1:-1] - luma[2:, 2:]) > threshold)]
        masks = [np.expand_dims(mask, axis=2) for mask in masks]
        sal = np.sum(np.concatenate(masks, axis=2), axis=2)

        mask = np.where(sal > 4, np.ones(sal.shape, dtype=np.uint8), np.zeros(sal.shape, dtype=np.uint8))
        mask = np.pad(mask, 1, mode='constant')
        mask = np.clip(guided_filter(luma / 255.0, 1.0 * mask, 8, 0.001), 0, 1.0)

        return mask

    @staticmethod
    def halo_control(luma, luma_gain, halo_pos=0.0, halo_neg=0.0):
        """
        Gain map halo control
        :param luma: np.array, image Y channel, np.uint8
        :param luma_gain: np.array, gain map for the Y channel
        :param halo_pos: float, control positive gain, "0" -> no halo control, "1" -> max strength
        :param halo_neg: float, control negative gain, "0" -> no halo control, "1" -> max strength
        :return:
        """
        b_pos = halo_pos * 55.0 + (1 - halo_pos) * 255.0
        k_pos = (255.0 - b_pos) / 255
        lut_pos = np.array([k_pos * i + b_pos for i in range(256)])

        b_neg = 0.0
        k_neg = (halo_neg * 155.0) / 255
        lut_neg = np.array([k_neg * i + b_neg for i in range(256)])

        gain_pos_cap = lut_pos[luma] - luma
        gain_neg_cap = lut_neg[luma] - luma

        output = np.where(luma_gain > 0, np.minimum(luma_gain, gain_pos_cap), np.maximum(luma_gain, gain_neg_cap))
        return output


if __name__ == "__main__":
    root_dir = 'C:/Users/linka/Desktop/2022_01_14/input/denoise_no_sdk_sharpen/'
    files = os.listdir(root_dir)
    for file in files:
        print(file)

        name = file.split('.')[0]
        path = os.path.join(root_dir, file)
        img = cv2.imread(path)

        y, u, v = rgb2yuv444p(img[:, :, 2], img[:, :, 1], img[:, :, 0])

        # For PolyBlur input, use strength = 4.0
        # For original input, use strength = 7.0
        y_sharp = USM.sharpen(y, strength=7.0, cut_off=0.0075, halo_pos=0.8, halo_neg=1.0, filtering=True)

        rgb = yuv444p2rgb(y_sharp, u, v)
        cv2.imwrite('C:/Users/linka/Desktop/' + name + '_ISP_denoise_kaimo_sharp.png', np.uint8(rgb[:, :, ::-1]))
