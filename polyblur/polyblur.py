from usm.color.rgb2gray import rgb2gray
import numpy as np
import cv2


class PolyBlur:
	""" Implementation of Google paper "Poly-blur: Removing Mild Blur by Polynomial Re-blurring" 2021 """

	@staticmethod
	def deblur(img: np.ndarray, iters=1, alpha=2, b=3, halo_remove=True):
		"""
		PolyBlur process function for general de-blurring

		:param img: (H, W, 1) Y-channel input degraded image , normalize to [0, 1] and float32
		:param iters: int, number of iterations
		:param alpha: int, alpha controls mid-frequency amplification
		:param b: int, controls noise amplification in blurred out areas (regularization of such)

		Returns:
			img_out: Image with poly-blur de-blurring applied
			psf (np.ndarray): Y-channel PSF, range[0, 1], normalized and np.float32
		"""
		if alpha == 0 and b == 0:
			return img, np.zeros_like(img)
		
		# Estimate psf
		psf = PolyBlur.estimate_psf(img)

		img_out = img.copy()
		for it in range(iters):
			img_out = PolyBlur.polyblur(img_out, psf, alpha, b)

		# halo remove
		if halo_remove:
			img_out, _ = PolyBlur.halo_remove(img, img_out)

		return img_out, psf

	@staticmethod
	def estimate_psf(img: np.ndarray):
		"""
		Estimate the psf of the input image

		Args:
			img: (H, W, 1) Y-channel image, normalize to [0, 1] and float32

		Returns:
			psf (np.ndarray): Y-channel PSF, range[0, 1], normalized and np.float32
		"""
		rho, sigma, theta = PolyBlur.estimate_gaussian_kernel(img[:, :, 0])

		psf = PolyBlur.gaussian_kernel2d(rho, sigma, theta, kz=7)
		psf = psf[:, :, np.newaxis]
		print(f"PSF shape {psf.shape}")
		return psf

	@staticmethod
	def polyblur(blur_im, psf, alpha=2, b=3):
		"""
		Equation 8 and equation 14 in the polyblur paper use order-3 polyblur
		for alpha=2, b=3, it is equivalent to polydeblur_d3_a2b3
		Other choices of (alpha, b) are (2,4),（8,1), (4,4), etc. (Fig.4)

		:param blur_im: Y-channel input image, normalize to [0, 1] and float32
		:param psf: np.ndarray, estimated gaussian blur kernel
		:param alpha: int, alpha controls mid-frequency amplification
		:param b: int, controls noise amplification in blurred out areas (regularization of such)
		:return:
		"""
		a3 = alpha / 2 - b + 2
		a2 = 3 * b - alpha - 6
		a1 = 5 - 3 * b + alpha / 2
		a0 = b

		out = np.zeros_like(blur_im)
		for c in range(blur_im.shape[-1]):
			v0 = a3 * blur_im[:, :, c]
			v1 = cv2.filter2D(v0, -1, psf[:, :, c]) + a2 * blur_im[:, :, c]
			v2 = cv2.filter2D(v1, -1, psf[:, :, c]) + a1 * blur_im[:, :, c]
			out[:, :, c] = cv2.filter2D(v2, -1, psf[:, :, c]) + a0 * blur_im[:, :, c]

		return np.clip(out, 0, 1)

	@staticmethod
	def halo_remove(blur_im, deblur_im, eps=1e-6):
		"""
		Halo removal based on the closed form method in Polyblur paper

		blur_im: input, blurry image (original input)
		deblur_im: deblur output (may have halo, i.e., gradient reversal)

		output: a blended image: z*blur_im+(1-z)*deblur_im
				the blending weight z
		"""
		gx, gy = np.gradient(blur_im[:, :, 0])
		gx1, gy1 = np.gradient(deblur_im[:, :, 0])

		v2 = gx * gx + gy * gy
		M = -(gx * gx1 + gy * gy1)

		M = np.maximum(M, 0)
		z = M / np.maximum(v2 + M, eps)

		z = z[..., np.newaxis]
		out = z * blur_im + (1 - z) * deblur_im

		return np.float32(out), z

	@staticmethod
	def estimate_gaussian_kernel(img: np.ndarray):
		"""
		Estimate the gaussian blur kernel based on the polyblur paper

		:param img: (H, W), input blur grayscale image, normalize to [0, 1] and float32
		:return: rho, sigma0, theta (params for a gaussian kernel. See gaussian_kernel2d)
		"""
		q = 0.0001
		n_angles = 12  # use 12 instead of 6, to avoid bicubic interp
		sigma_max = 4
		sigma_min = 0.3
		rho_max = 1
		rho_min = 0.15
		# calibrated params
		c = 89.8 / 255.0
		sigma_b = 0.6  # 0.764
		eps = 1e-8

		# normalize based on quantile (to remove outliers)
		x1, x2 = np.quantile(img, (q, 1 - q))
		img = (img - x1) / (x2 - x1)

		# find the maximum gradient in n_angles directions
		dx, dy = np.gradient(img)
		angles = np.linspace(0, np.pi, n_angles + 1)
		f = []
		for a in angles:
			u = dx * np.cos(a) - dy * np.sin(a)
			f.append(np.max(np.abs(u)))

		# find the direction with the minimum gradient
		# (for now, skip the interpolation step)
		fm = min(f)
		idx = f.index(fm)
		theta = angles[idx]
		# perpendicular angle
		idx = (idx + int(n_angles / 2)) % n_angles
		fd = f[idx]
		theta_d = angles[idx]

		# convert from f to the gaussian kernel param (Eq.13)
		fm = np.max([fm, eps])
		fd = np.max([fd, eps])
		sigma0 = np.sqrt(np.max([c * c / fm / fm - sigma_b * sigma_b, eps]))
		sigma1 = np.sqrt(np.max([c * c / fd / fd - sigma_b * sigma_b, eps]))

		sigma0 = np.clip(sigma0, sigma_min, sigma_max)
		sigma1 = np.clip(sigma1, sigma_min, sigma_max)
		rho = sigma1 / sigma0
		rho = np.clip(rho, rho_min, rho_max)

		return rho, sigma0, theta

	@staticmethod
	def gaussian_kernel2d(rho, sigma0, theta, kz=7):
		"""
		Gaussian blur kernel, size (2*kz+1) * (2*kz+1)

		:param rho: sigma1/sigma0 (ratio of the two principal axes) (rho=1 for isotropic gaussian)
		:param sigma0: variance along the first principal axis
		:param theta: angle of the first principal axis
		:param kz: kernel radius size
		:return: the gaussian kernel
		"""
		sigma1 = sigma0 * rho

		cs = np.cos(theta)
		ss = np.sin(theta)
		a0 = cs * cs / sigma0 / sigma0 + ss * ss / sigma1 / sigma1
		a1 = cs * ss / sigma0 / sigma0 * (1 / rho / rho - 1)
		a2 = ss * ss / sigma0 / sigma0 + cs * cs / sigma1 / sigma1

		ex = np.arange(-kz, kz + 1)
		xx, yy = np.meshgrid(ex, ex)
		tt = (a0 * xx * xx + 2 * a1 * xx * yy + a2 * yy * yy) / 2.0
		tt = np.exp(-tt)
		k = tt / np.sum(tt)
		return k


if __name__ == "__main__":
	path = '/Users/linkaimo/Desktop/0115_nightsight_0001_0124_msdct_NoSharp_NOISP_EE_HFG_G12.jpg'
	img = cv2.imread(path)
	img = img[:, :, ::-1] / 255.0

	result, psf = PolyBlur.deblur(img, iters=1, alpha=16, b=1)

	psf_out = (psf * 255).astype(np.uint8) * 10000
	cv2.imwrite("/Users/linkaimo/Desktop/psf.png", psf_out)

	cv2.imwrite("/Users/linkaimo/Desktop/out.png", np.uint8(255 * result[:, :, ::-1]))
